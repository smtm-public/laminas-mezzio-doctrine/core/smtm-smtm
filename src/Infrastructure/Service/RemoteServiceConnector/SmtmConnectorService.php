<?php

declare(strict_types=1);

namespace Smtm\Smtm\Infrastructure\Service\RemoteServiceConnector;

use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;
use Smtm\Base\Infrastructure\Helper\EnvHelper;
use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Smtm\Base\Infrastructure\Helper\OAuth2Helper;
use Smtm\Base\Infrastructure\Service\AbstractRemoteServiceConnector;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\RemoteServiceConnectorAuth;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class SmtmConnectorService extends AbstractRemoteServiceConnector
    implements ConfigAwareInterface
{

    use ConfigAwareTrait;

    public function requestAccessToken(
        string $providerUuid,
        string $authCode,
        string $redirectUri,
        ?string $codeVerifier = null,
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): array {
        $url = $this->config['ssoBaseUrl'] . '/auth-provider/token';

        if (isset($_GET['XDEBUG_SESSION_START'])) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION_START' => $_GET['XDEBUG_SESSION_START']]);
        }

        if (isset($_GET['XDEBUG_SESSION'])) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION' => $_GET['XDEBUG_SESSION']]);
        }

        if (!empty(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_CONFIG'))
            && !empty(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_START_WITH_REQUEST'))
            && EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_START_WITH_REQUEST') === 'trigger'
        ) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION' => base64_encode(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_CONFIG'))]);
        }

        $request = $this->createRequest('post', $url);
        $request = $this->setJsonBodyOnRequest(
            $request,
            [
                'providerUuid' => $providerUuid,
                'clientId' => $this->config['ssoClientId'],
                'clientSecret' => $this->config['ssoClientSecret'],
                'grantType' => OAuth2Helper::TOKEN_REQUEST_GRANT_TYPE_AUTHORIZATION_CODE,
                'code' => $authCode,
                'redirectUri' => $redirectUri,
                'codeVerifier' => $codeVerifier,
            ]
        );
        $response = $this->sendRequest($request, $options);

        return $this->parseResponse($response, HttpHelper::STATUS_CODE_CREATED);
    }

    public function requestAccessTokenRefresh(
        string $refreshToken,
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): array {
        $url = $this->config['ssoBaseUrl'] . '/auth-provider/token';

        if (isset($_GET['XDEBUG_SESSION_START'])) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION_START' => $_GET['XDEBUG_SESSION_START']]);
        }

        if (isset($_GET['XDEBUG_SESSION'])) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION' => $_GET['XDEBUG_SESSION']]);
        }

        if (!empty(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_CONFIG'))
            && !empty(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_START_WITH_REQUEST'))
            && EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_START_WITH_REQUEST') === 'trigger'
        ) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION' => base64_encode(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_CONFIG'))]);
        }

        $request = $this->createRequest(
            'post',
            $url
        );
        $request = $this->setJsonBodyOnRequest(
            $request,
            [
                'clientId' => $this->config['ssoClientId'],
                'clientSecret' => $this->config['ssoClientSecret'],
                'grantType' => OAuth2Helper::TOKEN_REQUEST_GRANT_TYPE_REFRESH_TOKEN,
                'refreshToken' => $refreshToken,
            ]
        );
        $response = $this->sendRequest($request, $options);

        return $this->parseResponse($response, HttpHelper::STATUS_CODE_CREATED);
    }

    public function requestAuthUserIndex(
        RemoteServiceConnectorAuth $auth,
        array $queryParams = [],
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): array {
        $url = $this->config['ssoBaseUrl'] . '/auth/user';

        if (isset($_GET['XDEBUG_SESSION_START'])) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION_START' => $_GET['XDEBUG_SESSION_START']]);
        }

        if (isset($_GET['XDEBUG_SESSION'])) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION' => $_GET['XDEBUG_SESSION']]);
        }

        if (!empty(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_CONFIG'))
            && !empty(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_START_WITH_REQUEST'))
            && EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_START_WITH_REQUEST') === 'trigger'
        ) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION' => base64_encode(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_CONFIG'))]);
        }

        $url = HttpHelper::urlAddQueryParams($url, $queryParams);
        $request = $this->createRequest('get', $url);
        $request = $this->processAuth($request, $options, $auth);

        $response = $this->sendRequest($request, $options);

        return $this->parseResponse($response);
    }

    public function requestAuthUserCreate(
        RemoteServiceConnectorAuth $auth,
        array $data,
        array $queryParams = [],
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): array {
        $url = $this->config['ssoBaseUrl'] . '/auth/user';

        if (isset($_GET['XDEBUG_SESSION_START'])) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION_START' => $_GET['XDEBUG_SESSION_START']]);
        }

        if (isset($_GET['XDEBUG_SESSION'])) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION' => $_GET['XDEBUG_SESSION']]);
        }

        if (!empty(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_CONFIG'))
            && !empty(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_START_WITH_REQUEST'))
            && EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_START_WITH_REQUEST') === 'trigger'
        ) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION' => base64_encode(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_CONFIG'))]);
        }

        $url = HttpHelper::urlAddQueryParams($url, $queryParams);
        $request = $this->createRequest('post', $url);
        $request = $this->processAuth($request, $options, $auth);

        $request = $this->setJsonBodyOnRequest(
            $request,
            $data
        );
        $response = $this->sendRequest($request, $options);

        return $this->parseResponse($response, HttpHelper::STATUS_CODE_CREATED);
    }

    public function requestAuthUserRead(
        RemoteServiceConnectorAuth $auth,
        string $userUuid,
        array $queryParams = [],
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): array {
        $url = $this->config['ssoBaseUrl'] . '/auth/user/' . $userUuid;

        if (isset($_GET['XDEBUG_SESSION_START'])) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION_START' => $_GET['XDEBUG_SESSION_START']]);
        }

        if (isset($_GET['XDEBUG_SESSION'])) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION' => $_GET['XDEBUG_SESSION']]);
        }

        if (!empty(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_CONFIG'))
            && !empty(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_START_WITH_REQUEST'))
            && EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_START_WITH_REQUEST') === 'trigger'
        ) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION' => base64_encode(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_CONFIG'))]);
        }

        $url = HttpHelper::urlAddQueryParams($url, $queryParams);
        $request = $this->createRequest('get', $url);
        $request = $this->processAuth($request, $options, $auth);

        $response = $this->sendRequest($request, $options);

        return $this->parseResponse($response, HttpHelper::STATUS_CODE_CREATED);
    }

    public function requestAuthUserPut(
        RemoteServiceConnectorAuth $auth,
        string $userUuid,
        array $data,
        array $queryParams = [],
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): array {
        $url = $this->config['ssoBaseUrl'] . '/auth/user/' . $userUuid;

        if (isset($_GET['XDEBUG_SESSION_START'])) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION_START' => $_GET['XDEBUG_SESSION_START']]);
        }

        if (isset($_GET['XDEBUG_SESSION'])) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION' => $_GET['XDEBUG_SESSION']]);
        }

        if (!empty(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_CONFIG'))
            && !empty(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_START_WITH_REQUEST'))
            && EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_START_WITH_REQUEST') === 'trigger'
        ) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION' => base64_encode(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_CONFIG'))]);
        }

        $url = HttpHelper::urlAddQueryParams($url, $queryParams);
        $request = $this->createRequest('put', $url);
        $request = $this->processAuth($request, $options, $auth);
        $request = $this->setJsonBodyOnRequest(
            $request,
            $data
        );

        $response = $this->sendRequest($request, $options);

        return $this->parseResponse($response);
    }

    public function requestAuthUserPatch(
        RemoteServiceConnectorAuth $auth,
        string $userUuid,
        array $data,
        array $queryParams = [],
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): array {
        $url = $this->config['ssoBaseUrl'] . '/auth/user/' . $userUuid;

        if (isset($_GET['XDEBUG_SESSION_START'])) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION_START' => $_GET['XDEBUG_SESSION_START']]);
        }

        if (isset($_GET['XDEBUG_SESSION'])) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION' => $_GET['XDEBUG_SESSION']]);
        }

        if (!empty(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_CONFIG'))
            && !empty(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_START_WITH_REQUEST'))
            && EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_START_WITH_REQUEST') === 'trigger'
        ) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION' => base64_encode(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_CONFIG'))]);
        }

        $url = HttpHelper::urlAddQueryParams($url, $queryParams);
        $request = $this->createRequest('patch', $url);
        $request = $this->processAuth($request, $options, $auth);
        $request = $this->setJsonBodyOnRequest(
            $request,
            $data
        );

        $response = $this->sendRequest($request, $options);

        return $this->parseResponse($response);
    }

    public function requestAuthUserDelete(
        RemoteServiceConnectorAuth $auth,
        string $userUuid,
        array $queryParams = [],
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): void {
        $url = $this->config['ssoBaseUrl'] . '/auth/user/' . $userUuid;

        if (isset($_GET['XDEBUG_SESSION_START'])) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION_START' => $_GET['XDEBUG_SESSION_START']]);
        }

        if (isset($_GET['XDEBUG_SESSION'])) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION' => $_GET['XDEBUG_SESSION']]);
        }

        if (!empty(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_CONFIG'))
            && !empty(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_START_WITH_REQUEST'))
            && EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_START_WITH_REQUEST') === 'trigger'
        ) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION' => base64_encode(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_CONFIG'))]);
        }

        $url = HttpHelper::urlAddQueryParams($url, $queryParams);
        $request = $this->createRequest('delete', $url);
        $request = $this->processAuth($request, $options, $auth);

        $response = $this->sendRequest($request, $options);

        $this->parseResponse($response, HttpHelper::STATUS_CODE_NO_CONTENT);
    }

    public function requestSsoUserPasswordReset(
        array $data,
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): mixed {
        $url = $this->config['ssoBaseUrl'] . '/sso/user/password-reset/initiate';

        if (isset($_GET['XDEBUG_SESSION_START'])) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION_START' => $_GET['XDEBUG_SESSION_START']]);
        }

        if (isset($_GET['XDEBUG_SESSION'])) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION' => $_GET['XDEBUG_SESSION']]);
        }

        if (!empty(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_CONFIG'))
            && !empty(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_START_WITH_REQUEST'))
            && EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_START_WITH_REQUEST') === 'trigger'
        ) {
            $url = HttpHelper::urlAddQueryParams($url, ['XDEBUG_SESSION' => base64_encode(EnvHelper::getEnvFromProcessOrSuperGlobal('XDEBUG_CONFIG'))]);
        }

        if(!isset($data['clientId'])) {
            $data['clientId'] = $this->config['ssoClientId'];
        }

        $request = $this->createRequest('post', $url);
        $request = $this->setJsonBodyOnRequest($request, $data);

        $response = $this->sendRequest($request, $options);

        return $this->parseResponse($response, HttpHelper::STATUS_CODE_NO_CONTENT);
    }
}
