<?php

declare(strict_types=1);

namespace Smtm\Smtm;

use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ConfigProvider
{
    #[ArrayShape(['dependencies' => 'array', 'smtm' => 'array'])] public function __invoke(): array
    {
        return [
            'dependencies' => include __DIR__ . '/../config/dependencies.php',
            'smtm' => include __DIR__ . '/../config/smtm.php',
        ];
    }
}
