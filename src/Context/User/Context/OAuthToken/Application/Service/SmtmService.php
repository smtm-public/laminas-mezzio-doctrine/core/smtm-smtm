<?php

declare(strict_types=1);

namespace Smtm\Smtm\Context\User\Context\OAuthToken\Application\Service;

use DateTimeImmutable;
use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Infrastructure\Helper\DateTimeHelper;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\RemoteServiceConnectorAuth;
use Smtm\Smtm\Infrastructure\Service\RemoteServiceConnector\SmtmConnectorService;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class SmtmService extends AbstractDbService
{
    public const AUTH_PROVIDER_ID = 1;

    public function createTokenDataFromAuthCode(
        string $providerUuid,
        string $code,
        string $redirectUri,
        ?string $codeVerifier = null,
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): array {
        /** @var SmtmConnectorService $smtmConnectorService */
        $smtmConnectorService = $this->infrastructureServicePluginManager->get(
            SmtmConnectorService::class
        );
        $jwtResponse = $data = $smtmConnectorService->requestAccessToken(
            $providerUuid,
            $code,
            $redirectUri,
            $codeVerifier,
            $options
        );
        $data['created'] = $jwtResponse['created'] !== null
            ? DateTimeImmutable::createFromFormat(
                DateTimeHelper::DEFAULT_FORMAT,
                $jwtResponse['created']
            )
            : null;
        $data['expires'] = $jwtResponse['expires'] !== null
            ? DateTimeImmutable::createFromFormat(
                DateTimeHelper::DEFAULT_FORMAT,
                $jwtResponse['expires']
            )
            : null;

        return $data;
    }

    public function createTokenDataFromRefresh(
        string $refreshToken,
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): array {
        /** @var SmtmConnectorService $smtmConnectorService */
        $smtmConnectorService = $this->infrastructureServicePluginManager->get(
            SmtmConnectorService::class
        );
        $jwtResponse = $data = $smtmConnectorService->requestAccessTokenRefresh(
            $refreshToken,
            $options
        );
        $data['created'] = $jwtResponse['created'] !== null
            ? DateTimeImmutable::createFromFormat(
                DateTimeHelper::DEFAULT_FORMAT,
                $jwtResponse['created']
            )
            : null;
        $data['expires'] = $jwtResponse['expires'] !== null
            ? DateTimeImmutable::createFromFormat(
                DateTimeHelper::DEFAULT_FORMAT,
                $jwtResponse['expires']
            )
            : null;

        return $data;
    }

    public function createClientJwtDataFromAuthCode(
        int $authProviderId,
        string $code,
        string $redirectUri,
        ?string $codeVerifier = null,
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): array {
        /** @var SmtmConnectorService $smtmConnectorService */
        $smtmConnectorService = $this->infrastructureServicePluginManager->get(
            SmtmConnectorService::class
        );
        $jwtResponse = $data = $smtmConnectorService->requestClientAccessToken(
            $authProviderId,
            $code,
            $redirectUri,
            $codeVerifier,
            $options
        );
        $data['expires'] = $jwtResponse['expires'] !== null
            ? DateTimeImmutable::createFromFormat(
                DateTimeHelper::DEFAULT_FORMAT,
                $jwtResponse['expires']
            )
            : null;

        return $data;
    }

    public function createClientJwtDataFromRefresh(
        string $jwt,
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): array
    {
        /** @var SmtmConnectorService $smtmConnectorService */
        $smtmConnectorService = $this->infrastructureServicePluginManager->get(
            SmtmConnectorService::class
        );
        $jwtResponse = $data = $smtmConnectorService->requestClientAccessTokenRefresh(
            $jwt,
            $options
        );
        $data['expires'] = $jwtResponse['expires'] !== null
            ? DateTimeImmutable::createFromFormat(
                DateTimeHelper::DEFAULT_FORMAT,
                $jwtResponse['expires']
            )
            : null;

        return $data;
    }

    #[ArrayShape([
        'provider' => 'int',
        'accessToken' => 'string',
        'refreshToken' => 'string',
        'idToken' => 'string',
        'expires' => '\DateTimeImmutable|false',
        'tokenType' => 'string',
        'scope' => 'string'
    ])] public function createOAuthTokenDataFromAuthCode(
        string $authClientId,
        string $authCode,
        string $redirectUri,
        ?string $codeVerifier = null,
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): array {
        /** @var SmtmConnectorService $smtmConnectorService */
        $smtmConnectorService = $this->infrastructureServicePluginManager->get(
            SmtmConnectorService::class
        );
        $tokenResponse = $smtmConnectorService->requestAuthenticationDataFromAuthCode(
            $authClientId,
            $authCode,
            $redirectUri,
            $codeVerifier,
            $options
        );
        // TODO: MD - replace with the standard expires_in
        $expires = DateTimeImmutable::createFromFormat(
            DateTimeHelper::DEFAULT_FORMAT,
            $tokenResponse['expires']
        );

        return [
            'provider' => static::AUTH_PROVIDER_ID,
            'accessToken' => $tokenResponse['access_token'],
            'refreshToken' => $tokenResponse['refresh_token'],
            'idToken' => $tokenResponse['id_token'],
            'expires' => $expires,
            'tokenType' => $tokenResponse['token_type'],
            'scope' => $tokenResponse['scope'],
        ];
    }

    #[ArrayShape([
        'provider' => 'int',
        'accessToken' => 'string',
        'refreshToken' => 'string',
        'idToken' => 'string',
        'expires' => '\DateTimeImmutable|false',
        'tokenType' => 'string',
        'scope' => 'string'
    ])] public function createOAuthTokenDataFromRefreshToken(
        string $authClientId,
        string $refreshToken,
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): array {
        /** @var SmtmConnectorService $smtmConnectorService */
        $smtmConnectorService = $this->infrastructureServicePluginManager->get(
            SmtmConnectorService::class
        );
        $tokenResponse = $smtmConnectorService->requestAuthenticationDataFromRefreshToken(
            $authClientId,
            $refreshToken,
            $options
        );
        // TODO: MD - replace with the standard expires_in
        $expires = DateTimeImmutable::createFromFormat(
            DateTimeHelper::DEFAULT_FORMAT,
            $tokenResponse['expires']
        );

        return [
            'provider' => static::AUTH_PROVIDER_ID,
            'accessToken' => $tokenResponse['access_token'],
            'refreshToken' => $tokenResponse['refresh_token'],
            'idToken' => $tokenResponse['id_token'],
            'expires' => $expires,
            'tokenType' => $tokenResponse['token_type'],
            'scope' => $tokenResponse['scope'],
        ];
    }

    public function authUserIndex(
        RemoteServiceConnectorAuth $auth,
        array $queryParams = [],
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): array {
        /** @var SmtmConnectorService $smtmConnectorService */
        $smtmConnectorService = $this->infrastructureServicePluginManager->get(
            SmtmConnectorService::class
        );
        $response = $smtmConnectorService->requestAuthUserIndex(
            $auth,
            $queryParams,
            $options
        );

        return $response;
    }

    public function authUserCreate(
        RemoteServiceConnectorAuth $auth,
        array $data,
        array $queryParams = [],
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): array {
        /** @var SmtmConnectorService $smtmConnectorService */
        $smtmConnectorService = $this->infrastructureServicePluginManager->get(
            SmtmConnectorService::class
        );
        $response = $smtmConnectorService->requestAuthUserCreate(
            $auth,
            $data,
            $queryParams,
            $options
        );

        return $response;
    }

    public function authUserRead(
        RemoteServiceConnectorAuth $auth,
        string $userUuid,
        array $data,
        array $queryParams = [],
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): array {
        /** @var SmtmConnectorService $smtmConnectorService */
        $smtmConnectorService = $this->infrastructureServicePluginManager->get(
            SmtmConnectorService::class
        );
        $response = $smtmConnectorService->requestAuthUserRead(
            $auth,
            $userUuid,
            $queryParams,
            $options
        );

        return $response;
    }

    public function authUserPut(
        RemoteServiceConnectorAuth $auth,
        string $userUuid,
        array $data,
        array $queryParams = [],
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): array {
        /** @var SmtmConnectorService $smtmConnectorService */
        $smtmConnectorService = $this->infrastructureServicePluginManager->get(
            SmtmConnectorService::class
        );
        $response = $smtmConnectorService->requestAuthUserPut(
            $auth,
            $userUuid,
            $data,
            $queryParams,
            $options
        );

        return $response;
    }

    public function authUserPatch(
        RemoteServiceConnectorAuth $auth,
        string $userUuid,
        array $data,
        array $queryParams = [],
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): array {
        /** @var SmtmConnectorService $smtmConnectorService */
        $smtmConnectorService = $this->infrastructureServicePluginManager->get(
            SmtmConnectorService::class
        );
        $response = $smtmConnectorService->requestAuthUserPatch(
            $auth,
            $userUuid,
            $data,
            $queryParams,
            $options
        );

        return $response;
    }

    public function authUserDelete(
        RemoteServiceConnectorAuth $auth,
        string $userUuid,
        array $data,
        array $queryParams = [],
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): void {
        /** @var SmtmConnectorService $smtmConnectorService */
        $smtmConnectorService = $this->infrastructureServicePluginManager->get(
            SmtmConnectorService::class
        );
        $smtmConnectorService->requestAuthUserDelete(
            $auth,
            $userUuid,
            $queryParams,
            $options
        );
    }

    public function ssoUserPasswordResetInitiate(
        array $data,
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): mixed
    {
        /** @var SmtmConnectorService $smtmConnectorService */
        $smtmConnectorService = $this->infrastructureServicePluginManager->get(
            SmtmConnectorService::class
        );

        return $smtmConnectorService->requestSsoUserPasswordReset($data);
    }
}
