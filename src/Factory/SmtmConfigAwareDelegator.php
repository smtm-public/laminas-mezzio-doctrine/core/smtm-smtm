<?php

declare(strict_types=1);

namespace Smtm\Smtm\Factory;

use Smtm\Base\Factory\ConfigAwareDelegator;

class SmtmConfigAwareDelegator extends ConfigAwareDelegator
{
    protected array|string|null $configKey = 'smtm';
}
