<?php

declare(strict_types=1);

namespace Smtm\Smtm;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

return [
    'logger' => [
        'name' => EnvHelper::getEnvFromProcessOrSuperGlobal(
            'SMTM_SERVICE_INTEGRATION_SMTM_REMOTE_SERVICE_CONNECTOR_LOGGER_NAME'
        ),
    ],
];
