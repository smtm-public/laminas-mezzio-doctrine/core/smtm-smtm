<?php

declare(strict_types=1);

namespace Smtm\Microsoft;

use Smtm\Base\Infrastructure\Laminas\Log\LoggerAwareInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Smtm\Factory\SmtmConfigAwareDelegator;
use Smtm\Smtm\Infrastructure\Service\RemoteServiceConnector\SmtmConnectorService;
use Psr\Container\ContainerInterface;

return [
    'delegators' => [
        InfrastructureServicePluginManager::class => [
            function (
                ContainerInterface $container,
                $name,
                callable $callback,
                array $options = null
            ) {
                /** @var InfrastructureServicePluginManager $infrastructureServicePluginManager */
                $infrastructureServicePluginManager = $callback();

                return $infrastructureServicePluginManager->configure(
                    [
                        'delegators' => [
                            SmtmConnectorService::class => [
                                SmtmConfigAwareDelegator::class,
                                function(ContainerInterface $container, $name, callable $callback, array $options = null) {
                                    /** @var LoggerAwareInterface $object */
                                    $object = $callback();

                                    $loggerName =
                                        (
                                            (is_array($options) ? $options['logger']['name'] : null)
                                            ?? $container
                                                ->get('config')['smtm']['infrastructure']['remote_service_connector']['logger']['name']

                                        ) ?: null;
                                    $loggerName !== null
                                    && $object->setLogger(
                                        $container->get(InfrastructureServicePluginManager::class)->get($loggerName)
                                    );

                                    return $object;
                                },
                            ],
                        ],
                    ]
                );
            }
        ],
    ],
];
