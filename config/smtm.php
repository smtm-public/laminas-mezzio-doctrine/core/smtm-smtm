<?php

declare(strict_types=1);

namespace Smtm\Smtm;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-smtm')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../',
        '.env.smtm.smtm-smtm'
    );
    $dotenv->load();
}

return [
    'ssoPermitRequestDebugSession' =>
        EnvHelper::getEnvFromProcessOrSuperGlobal(
                'SMTM_SMTM_SSO_PERMIT_REQUEST_DEBUG_SESSION',
        ),
    'ssoBaseUrl' =>
        EnvHelper::getEnvFromProcessOrSuperGlobal(
            'SMTM_SMTM_SSO_BASE_URL'
        ),
    'ssoAuthenticationBasicUsername' =>
        EnvHelper::getEnvFromProcessOrSuperGlobal(
            'SMTM_SMTM_SSO_AUTHENTICATION_BASIC_USERNAME'
        ),
    'ssoAuthenticationBasicPassword' =>
        EnvHelper::getEnvFromProcessOrSuperGlobal(
            'SMTM_SMTM_SSO_AUTHENTICATION_BASIC_PASSWORD'
        ),
    'authClientIdToSsoCredentials' =>
        json_decode(
            EnvHelper::getEnvFromProcessOrSuperGlobal(
                'SMTM_SMTM_AUTH_CLIENT_ID_TO_SSO_CREDENTIALS',
                '[]'
            ),
            true
        ),
    'ssoClientId' =>
        EnvHelper::getEnvFromProcessOrSuperGlobal(
            'SMTM_SMTM_SSO_CLIENT_ID'
        ),
    'ssoClientSecret' =>
        EnvHelper::getEnvFromProcessOrSuperGlobal(
            'SMTM_SMTM_SSO_CLIENT_SECRET'
        ),
    'ssoPublicKeyFilePath' =>
        __DIR__ . '/../../../../data/keys/'
        . EnvHelper::getEnvFromProcessOrSuperGlobal(
            'SMTM_SMTM_SSO_PUBLIC_KEY_FILE_NAME'
        ),

    'authServicePermitRequestDebugSession' =>
        EnvHelper::getEnvFromProcessOrSuperGlobal(
            'SMTM_SMTM_AUTH_SERVICE_PERMIT_REQUEST_DEBUG_SESSION'
        ),
    'authServiceBaseUrl' =>
        EnvHelper::getEnvFromProcessOrSuperGlobal(
            'SMTM_SMTM_AUTH_SERVICE_URL'
        ),
    'authServiceClientId' =>
        EnvHelper::getEnvFromProcessOrSuperGlobal(
            'SMTM_SMTM_AUTH_SERVICE_CLIENT_ID',
        ),
    'authServiceClientSecret' =>
        EnvHelper::getEnvFromProcessOrSuperGlobal(
            'SMTM_SMTM_AUTH_SERVICE_CLIENT_SECRET',
        ),
    'authServicePublicKeyFilePath' =>
        __DIR__ . '/../../../../data/keys/'
        . EnvHelper::getEnvFromProcessOrSuperGlobal(
            'SMTM_SMTM_AUTH_SERVICE_PUBLIC_KEY_FILE_NAME',
        ),

    'infrastructure' => [
        'remote_service_connector' => include __DIR__ . '/infrastructure/remote_service_connector.php',
    ],
];
